// init new tree
async function initTree(response, container, paths) {
  new Tree(await response, container, paths);
}



// COMMON
function _cloneObject(object) {
  const clone = Object.create(null);
  for (let k in object) {
    clone[k] = Array.isArray(object[k]) ? _cloneArray(object[k]) : object[k];
  }
  return clone;
}
function _cloneArray(array) {
  const clone = array.map(v => v);
  return clone;
}



// - - - - CLASS NODE - - - - //
class Node {
  constructor(id, data) {
    this.id     = id;
    this.parent = data.parent;

    this._createNode(data);
  }

  // creating DOM node
  _createNode(data) {
    // main node with or without image
    this.self                = document.createElement('div');
    this.self.className      = 'binary-view-node';
    this.self.dataset.nodeId = this.id;
    this.self.dataset.depth  = data.depth;
    this.self.className      = 'binary-view-node';

    // inner border
    const selfInnerBorder = document.createElement('span');
    // avatar
    const svgAvatar = document.createElement('img');
    svgAvatar.src   = '/assets/avatar.svg';
    // name
    this.text = document.createElement('p');
    this.text.textContent        = data.name;
    this.text.dataset.nodeTextId = this.id;

    this.self.appendChild(selfInnerBorder);
    this.self.appendChild(svgAvatar);

    if (!data.isPaid) this.self.classList.add('inactive');
  }
}


// - - - - CLASS TREE - - - - //
class Tree {
  constructor(response, container, paths) {
    this.paths      = paths;
    this.tabs       = document.querySelector('.tree-selectors');
    this.button     = document.querySelector('.tree-button-add');
    this.addNewForm = document.getElementById('add-new');

    this.container = container;
    this.rawData   = response;             // flat array from server
    this.root      = null;                 // root id
    this.hash      = Object.create(null);  // hash table - { id: { parent, children } }
    this.maxLevels = 4;                    // max levels at a time

    // tree container
    this.container.appendChild(document.createElement('div'));
    // links container
    this.container.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));

    // this.root, this.hash
    this.setHash(this.rawData);
    this.setSides();

    // select active tab
    this.tabs.children[this.hash[this.root].active_branch].classList.add('active');
    // set listeners
    this.tabs.addEventListener('click', this.setSideTab.bind(this));
    this.button.addEventListener('click', this.openForm.bind(this));

    const { rootId, hashPart } = this.getDescendants(this.root);
    this.appendNodes(hashPart, rootId);
    // // HACK: because transition 200ms
    setTimeout(() => this.appendLinks(hashPart, rootId), 200);
  }

  // hash table
  setHash(array) {
    this.root = null;

    // add cell to table
    const add = (id, data) => {
      this.hash[id]          = Object.create(null);
      this.hash[id].children = new Array();
      Object.assign(this.hash[id], data);
    };

    array.forEach((v, i, arr) => {
      if (!this.hash[v.id]) add(v.id, v);
      // finding root node
      if (v.parent == null || v.parent == '') {
        if (this.root) console.error(`There is more than one root node. rootIds: [${this.root}, ${v.id}]`);
        else this.root = v.id;
      }
      // if node has parent and the parent does not exist yet
      if (v.parent && !this.hash[v.parent]) add(v.parent, arr.find(p => p.id == v.parent));
      // if parent node exist
      if (this.hash[v.parent]) {
        if (this.hash[v.parent].children.length == 2) {
          console.error(`id: ${v.parent} has more than two child nodes. Next child id ${v.id}`);
        } else {
          this.hash[v.parent].children.push(v.id);
          if (this.hash[v.parent].children.length == 2) {
            this.hash[v.parent].children.sort((a, b) => this.hash[a].branch - this.hash[b].branch);
          }
        }
      }
    });

    if (!this.root) console.error('There is no root node or a root node has parent that does not contains in the data.');

    delete this.rawData;
  }

  // set root sides and balance
  setSides() {
    this.hash[this.root].balanceLeft     = 0;  // balance in the left branch
    this.hash[this.root].balanceRight    = 0;  // balance in the right branch
    this.hash[this.root].nodesCountLeft  = 0;  // number of nodes in the left branch
    this.hash[this.root].nodesCountRight = 0;  // number of nodes in the right branch

    // helper
    const setSide = id => {
      this.hash[id].children.forEach(v => {
        this.hash[v].rootSide = this.hash[id].rootSide;
        if (this.hash[id].rootSide == 0) {
          this.hash[this.root].balanceLeft += this.hash[v].balance;
          this.hash[this.root].nodesCountLeft++;
        } else {
          this.hash[this.root].balanceRight += this.hash[v].balance;
          this.hash[this.root].nodesCountRight++;
        }
        if (this.hash[v].children.length > 0) setSide(v);
      });
    };

    // if the root does not have children
    if (this.hash[this.root].children.length == 0) return;
    // if there is only one child
    else if (this.hash[this.root].children.length == 1) {
      let id = this.hash[this.root].children[0];
      if (this.hash[id].branch == 0) {
        this.hash[id].rootSide = 0;
        this.hash[this.root].balanceLeft += this.hash[id].balance;
        this.hash[this.root].nodesCountLeft++;
      } else {
        this.hash[id].rootSide = 1;
        this.hash[this.root].balanceRight += this.hash[id].balance;
        this.hash[this.root].nodesCountRight++;
      }
      setSide(id);
      // if two children
    } else {
      const leftId  = this.hash[this.root].children[0];
      const rightId = this.hash[this.root].children[1];

      this.hash[leftId].rootSide  = 0;
      this.hash[rightId].rootSide = 1;
      this.hash[this.root].nodesCountLeft++;
      this.hash[this.root].nodesCountRight++;

      setSide(leftId);
      setSide(rightId);
    }

    this.hash[this.root].balanceLeft  = Math.round(this.hash[this.root].balanceLeft * 100) / 100;
    this.hash[this.root].balanceRight = Math.round(this.hash[this.root].balanceRight * 100) / 100;
  }

  getDescendants(rootId) {
    let   depth    = 0;
    const hashPart = Object.create(null);

    const setStructure = (id, depth) => {
      hashPart[id] = _cloneObject(this.hash[id]);
      hashPart[id].depth = depth;
      hashPart[id].hasMoreLevels = id == rootId && id !== this.root ? true : false;
      hashPart[id].node = new Node(id, hashPart[id]);

      if (++depth >= this.maxLevels) {
        if (hashPart[id].children.length > 0) hashPart[id].hasMoreLevels = true;
        hashPart[id].children = new Array();
      } else this.hash[id].children.forEach(v => setStructure(v, depth));
    };

    setStructure(rootId, depth);

    return {rootId, hashPart};
  }

  getAncestors(deepId) {
    let depth = 3;
    let id = deepId;
    while (depth !== 0) {
      id = this.hash[id].parent;
      depth--;
    }

    return this.getDescendants(id);
  }

  // creating view
  appendNodes(hashPart, rootId) {
    if (rootId !== this.root) {
      hashPart[rootId].node.self.style.cursor = 'pointer';
      hashPart[rootId].node.self.classList.add('has-more');
    }

    const appendNode = (container, id, index, array) => {
      // self container
      const selfContainer = document.createElement('div');
      selfContainer.className = 'binary-view-container';
      // if there is only one child
      if (index == 0 && !array[1]) selfContainer.style.width = '100%';
      // info block
      if (id == this.root) {
        const info = document.createElement('div');
        info.className = 'binary-view-info';
        info.innerHTML = `
          <div>${hashPart[id].balanceLeft.toLocaleString('ru-RU')} ₽</div>
          <div>${hashPart[id].balanceRight.toLocaleString('ru-RU')} ₽</div>
          <div>${hashPart[id].nodesCountLeft.toLocaleString('ru-RU')} человек</div>
          <div>${hashPart[id].nodesCountRight.toLocaleString('ru-RU')} человек</div>
        `;
        selfContainer.appendChild(info);
      }
      // if node has children but they dont contains in the view
      if (hashPart[id].hasMoreLevels) {
        hashPart[id].node.self.style.cursor = 'pointer';
        hashPart[id].node.self.classList.add('has-more');
      }
      selfContainer.appendChild(hashPart[id].node.self);
      selfContainer.appendChild(hashPart[id].node.text);
      container.appendChild(selfContainer);

      this.calculateSize(hashPart, id);

      if (hashPart[id].children.length > 0) hashPart[id].children.forEach((v, i, arr) => appendNode(selfContainer, v, i, arr));
    };

    appendNode(this.container.children[0], rootId);

    // add listeners
    const nodeClass = depth => `.binary-view-node.has-more[data-depth="${depth}"]`;
    document.querySelectorAll(nodeClass(3)).forEach(v => {
      v.addEventListener('click', this.openDescendant.bind(this));
    });
    if (document.querySelector(nodeClass(0))) document.querySelector(nodeClass(0)).addEventListener('click', this.openAncestors.bind(this));
  }

  // calculating sizes for nodes
  calculateSize(hashPart, id) {
    const self        = hashPart[id].node.self;
    const selfBorder  = parseFloat(getComputedStyle(self).borderTopWidth);
    const innerBorder = parseFloat(getComputedStyle(self.children[0]).borderTopWidth);
    const ratio       = document.documentElement.clientWidth > 500 ? 12 : 13;

    if (!hashPart[id].isPaid) self.classList.add('inactive');
    self.style.width       = `${self.getBoundingClientRect().width - hashPart[id].depth % 4 * ratio}px`;
    self.style.height      = `${self.getBoundingClientRect().height - hashPart[id].depth % 4 * ratio}px`;
    self.style.borderWidth = `${selfBorder - hashPart[id].depth % 4}px`;

    if (hashPart[id].image) {
      self.style.backgroundImage     = `url('${hashPart[id].image}')`;
      self.children[0].style.display = 'none';
      self.children[1].style.display = 'none';
    } else self.children[0].style.borderWidth = `${innerBorder - hashPart[id].depth % 4}px`;

    self.children[1].style.width  = `calc(100% + ${(selfBorder - hashPart[id].depth % 4) * 2}px)`;
    self.children[1].style.height = `calc(100% + ${(selfBorder - hashPart[id].depth % 4) * 2}px)`;
    self.children[1].style.top    = `-${selfBorder - hashPart[id].depth % 4}px`;
    self.children[1].style.left   = `-${selfBorder - hashPart[id].depth % 4}px`;
  }

  // creating links
  appendLinks(hashPart, rootId) {
    const containerProp = this.container.getBoundingClientRect();
    const scroll        = document.documentElement.scrollTop;

    function percent(num, prop) {
      return num * 100 / containerProp[prop];
    }

    const leftLinks  = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    const rightLinks = document.createElementNS('http://www.w3.org/2000/svg', 'g');

    this.container.children[1].appendChild(leftLinks);
    this.container.children[1].appendChild(rightLinks);
    this.container.children[1].children[this.hash[this.root].active_branch].classList.add('active');

    const appendLink = id => {
      const parentProp = hashPart[id].node.self.getBoundingClientRect();
      const x1         = percent(parentProp.left - containerProp.left + parentProp.width / 2, 'width');
      const y1         = percent((parentProp.top + scroll) - (containerProp.top + scroll) + parentProp.height / 2, 'height');

      if (hashPart[id].children.length > 0) {
        hashPart[id].children.forEach(v => {
          const childProp = hashPart[v].node.self.getBoundingClientRect();
          const link      = document.createElementNS('http://www.w3.org/2000/svg', 'line');
          const x2        = percent(childProp.left - containerProp.left + childProp.width / 2, 'width');
          const y2        = percent((childProp.top + scroll) - (containerProp.top + scroll) + childProp.height / 2, 'height');

          link.setAttribute('x1', `${x1}%`);
          link.setAttribute('y1', `${y1}%`);
          link.setAttribute('x2', `${x2}%`);
          link.setAttribute('y2', `${y2}%`);

          if (hashPart[v].rootSide == 0) leftLinks.appendChild(link);
          else rightLinks.appendChild(link);
          appendLink(v);
        });
      }

      // dots
      if (hashPart[id].hasMoreLevels) {
        const circleTwo    = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        const circleLeft   = percent(parentProp.left - containerProp.left + parentProp.width / 2 - 15, 'width');
        const circleCenter = percent(parentProp.left - containerProp.left + parentProp.width / 2, 'width');
        const circleRight  = percent(parentProp.left - containerProp.left + parentProp.width / 2 + 15, 'width');
        let circlesTop;
        // the lowest depth in the view
        if (hashPart[id].depth !== 0) circlesTop = percent((parentProp.top + scroll) - (containerProp.top + scroll) + parentProp.height + 15, 'height');
        // the highest depth in the view
        else circlesTop = percent((parentProp.top + scroll) - (containerProp.top + scroll) - 15, 'height');

        circleTwo.setAttribute('cx', `${circleCenter}%`);
        circleTwo.setAttribute('cy', `${circlesTop}%`);
        circleTwo.setAttribute('r', '5');

        const circleOne   = circleTwo.cloneNode();
        const circleThree = circleTwo.cloneNode();

        circleOne.setAttribute('cx', `${circleLeft}%`);
        circleThree.setAttribute('cx', `${circleRight}%`);

        this.container.children[1].appendChild(circleTwo);
        this.container.children[1].appendChild(circleOne);
        this.container.children[1].appendChild(circleThree);
      }
    };

    appendLink(rootId);
  }

  // remove nodes from view
  clearTree() {
    // helper
    const clearDeep = node => {
      for (let i = node.children.length; i > 0; i--) node.removeChild(node.children[i - 1]);
    };
    clearDeep(this.container.children[0]); // clear nodes
    clearDeep(this.container.children[1]); // clear links
  }



  // select tab
  setSideTab({ target }) {
    if (getComputedStyle(this.addNewForm).display == 'block') return;
    if (target.classList.contains('active') || target.classList.contains('tree-selectors')) return;

    for (let i = 0; i < this.tabs.children.length; i++) {
      this.tabs.children[i].classList.toggle('active');
      this.container.children[1].children[i].classList.toggle('active');
    }
    this.hash[this.root].active_branch = this.hash[this.root].active_branch == 0 ? 1 : 0;

    fetch(this.paths.postActiveBranch, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body:    JSON.stringify({
        id:            this.root,
        active_branch: +target.dataset.branch,
        act:           'change_branch'
      })
    }).catch(rej => console.log(rej));
  }

  openForm() {
    // ADD NEW USER
    const addNewUser = id => {
      if (id) {
        this.addNewForm.style.display = 'block';
        this.addNewForm.children[1].setAttribute('action', `${this.paths.postNewUser}/${id}`);
      } else {
        this.addNewForm.style.display = 'none';
        this.addNewForm.children[1].childNodes.forEach(n => {
          if (n.nodeName == 'INPUT' && n.getAttribute('type') !== 'submit') n.value = '';
        });
      }
    };
    if (getComputedStyle(this.addNewForm).display == 'block') addNewUser();
    else addNewUser(this.root);
  }

  // open next levels
  openDescendant({ target }) {
    if (getComputedStyle(this.addNewForm).display == 'block') return;

    let parent = target;
    while(!parent.classList.contains('binary-view-node')) parent = parent.parentNode;
    this.clearTree();

    const id = +parent.dataset.nodeId;
    const { rootId, hashPart } = this.getDescendants(id);
    this.appendNodes(hashPart, rootId);
    // HACK: because transition 200ms
    setTimeout(() => this.appendLinks(hashPart, rootId), 200);
  }

  // open previous levels
  openAncestors({ target }) {
    if (getComputedStyle(this.addNewForm).display == 'block') return;

    let parent = target;
    while(!parent.classList.contains('binary-view-node')) parent = parent.parentNode;
    this.clearTree();

    const id = +parent.dataset.nodeId;
    const { rootId, hashPart } = this.getAncestors(id);
    this.appendNodes(hashPart, rootId);
    // HACK: because transition 200ms
    setTimeout(() => this.appendLinks(hashPart, rootId), 200);
  }
}


export default initTree;
import initTree from './binary-tree.js';



const paths = {
  getBinaryData:    '/data/test-binary-data.json',
  postNewUser:      '/user/add',
  postActiveBranch: '/user/branch'
};



/* - - - - - - - WORK WITH DOM - - - - - - - */
// LOGIN
const loginForm            = document.getElementById('login');
const recoveryForm         = document.getElementById('recovery');
const recoveryButton       = document.getElementById('recovery-button');
const recoveryButtonCancel = document.getElementById('recovery-button-cancel');

// MENU
const menuBurger = document.getElementById('menu-burger');
const menu       = document.querySelector('.menu');
let   menuState  = false;

// CONTENT / FEATHERS
const contentBlock  = document.querySelector('.content-block');
const feathersBlock = document.querySelector('.feathers');

// CONTENT / USER INFO
const userBlock = document.querySelectorAll('.content-block-user');

// CONTENT / PAYMENT
const paymentBlock = document.querySelectorAll('.content-block-payment');

// CONTENT / BANK
const bankBlock = document.querySelectorAll('.content-block-bank');

// CONTENT / TEAM
const addNewForm = document.getElementById('add-new');



// LOGIN/RECOVERY
if (loginForm) {
  recoveryForm.style.display = 'none';

  recoveryButton.addEventListener('click', () => {
    loginForm.style.display = 'none';
    recoveryForm.style.display = '';
  });
  recoveryButtonCancel.addEventListener('click', () => {
    loginForm.style.display = '';
    recoveryForm.style.display = 'none';
  });
}



// BURGER
function showMenu() {
  if (menuState) menu.style.left = '';
  else menu.style.left = '0';
  menuState = !menuState;
}

if (menuBurger) {
  menuBurger.addEventListener('click', showMenu);
}



// FEATHERS
function setFeatherHeight() {
  const props                      = contentBlock.getBoundingClientRect();
        feathersBlock.style.width  = `${props.width}px`;
        feathersBlock.style.height = `${props.height}px`;
        feathersBlock.style.top    = `${props.top}px`;
}

if (feathersBlock) {
  setFeatherHeight();
  window.addEventListener('resize', setFeatherHeight);
}



// USER INFO
function setUserBlock({ target }) {
  if (target.id == 'change-user') {
    userBlock[0].style.display = 'none';
    userBlock[1].style.display = '';
  } else {
    userBlock[0].style.display = '';
    userBlock[1].style.display = 'none';
  }
}

if (userBlock.length > 0) {
  userBlock[0].style.display = '';
  userBlock[1].style.display = 'none';
  document.getElementById('change-user').addEventListener('click', setUserBlock);
  document.getElementById('cancel-user').addEventListener('click', setUserBlock);
}



// PAYMENT INFO
function setPaymentBlock({ target }) {
  if (target.id == 'my-payments') {
    paymentBlock[0].style.display = 'none';
    paymentBlock[1].style.display = '';
  } else {
    paymentBlock[0].style.display = '';
    paymentBlock[1].style.display = 'none';
  }
}

if (paymentBlock.length > 0) {
  paymentBlock[0].style.display = '';
  paymentBlock[1].style.display = 'none';
  document.getElementById('my-payments').addEventListener('click', setPaymentBlock);
  document.getElementById('my-payments-back').addEventListener('click', setPaymentBlock);
}



// PAYMENT INFO
function setBankBlock({ target }) {
  if (target.id == 'my-requests') {
    bankBlock[0].style.display = 'none';
    document.querySelector('.feathers').style.display = 'none';
    bankBlock[1].style.display = '';
  } else {
    bankBlock[0].style.display = '';
    document.querySelector('.feathers').style.display = '';
    bankBlock[1].style.display = 'none';
  }
}

if (bankBlock.length > 0) {
  bankBlock[0].style.display = '';
  document.querySelector('.feathers').style.display = '';
  bankBlock[1].style.display = 'none';
  document.getElementById('my-requests').addEventListener('click', setBankBlock);
  document.getElementById('my-requests-back').addEventListener('click', setBankBlock);
}
/* - - - - - - - WORK WITH DOM END - - - - - - - */



/* - - - - - - - GET DATA FUNCTION - - - - - - - */
// BINARY VIEW
async function getData(path) {
  try {
    const res = await fetch(path);
    return await res.json();
  }
  catch (rej) {
    return console.log(rej);
  }
}
/* - - - - - - - GET DATA FUNCTION END - - - - - - - */



/* - - - - - - - TREE VIEW SECTION - - - - - - - */
const treeContainer = document.querySelector('.binary-view');

if (addNewForm) {
  document.getElementById('cancel-add-new').addEventListener('click', () => addNewForm.style.display = 'none');
}

// NEW BINARY TREE VIEW
if (treeContainer) initTree(getData(paths.getBinaryData), treeContainer, paths);
/* - - - - - - - TREE VIEW SECTION END - - - - - - - */